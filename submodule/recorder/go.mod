module chainmaker.org/chainmaker/recorder

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/smartystreets/goconvey v1.7.2
	gopkg.in/yaml.v2 v2.2.8
	gorm.io/driver/mysql v1.4.3
	gorm.io/driver/sqlite v1.4.4
	gorm.io/gorm v1.24.0
)
